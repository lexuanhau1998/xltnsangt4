import numpy as np
import matplotlib.pyplot as plt


# ví dụ 1:
# A = .8
# f = 5
# t = np.arange(0,1,.01) #chia đều khoảng cách trên trục x
# phi = np.pi/4#pi=3.14
# x = A*np.cos(2*np.pi*f*t+phi)#công thức tính lượng giác cos
# plt.plot(t,x)#vẽ đồ thị
# plt.axis([0,1,-1,1])#giới hạn đồ thị 0 ->1 trục x, -1->1 trục y
# plt.xlabel('time in seconds')# ghi tên dưới trục x
# plt.ylabel('amplitude')# ghi tên ở trục y
# plt.show()# hiển thị hình



# ví dụ 2
# A= .65 #kiểu dữ liệu float
# fs = 100 # kiểu integer
# samples = 100
# f = 5
# phi = 0
# n= np.arange(samples)# chia đều khoảng cách = 100
# T = 1.0/fs
# y = A*np.cos(2*np.pi*f*n*T + phi)# hàng tính cos
# plt.plot(y)# vẽ đồ thị y
# plt.axis([0,100,-1,1])# giới hạn đồ thị 0 ->100 trục x, -1->1 trục y
# plt.xlabel('sample index')# ghi tên dưới trục x
# plt.ylabel('amplitude')# ghi tên ở trục y
# plt.show()# hiển thị hình


# ví dụ 3:
# A = .8
# N = 100
# f = 5
# phi = 0
# n = np.arange(N)# chia đều khoảng cách ở trục x
# y = A*np.cos(2*np.pi*f*n/N + phi)# hàm tính cos
# plt.plot(y)# hàm vẽ đồ thị y
# plt.axis([0,100,-1,1])# giới hạn đồ thị 0 ->100 trục x, -1->1 trục y
# plt.xlabel('sample index')# ghi tên dưới trục x
# plt.ylabel('amplitude')# ghi tên ở trục y
# plt.show()# hiển thị hình



# # ví dụ 4:
# f = 3# kiểu integer
# t = np.arange(0,1,.01)# chia đều khoảng cách
# phi = 0
# x = np.exp(1j*(2*np.pi*f*t + phi))# hàm tính e
# xim = np.imag(x)# lấy phần thực
# plt.figure(1)# hàm chứ các thành phần của hình vẽ
# plt.plot(t,np.real(x))# vẽ đồ thị vecto từ t đến phần ảo của x
# plt.plot(t,xim)# vẽ đồ thị vecto từ t đến phần thực của xim
# plt.axis([0,1,-1.1,1.1])# giới hạn đồ thị 0 ->1 trục x, -1.1->1.1 trục y
# plt.xlabel('time in seconds')# ghi tên dưới trục x
# plt.ylabel('amplitude')# ghi tên ở trục y
# plt.show()#hiển thị hình



# # ví dụ 5:
# f = 3
# N = 100
# fs = 100
# n = np.arange(N)# chia đều khoảng cách
# T = 1.0/fs
# t = N*T
# phi = 0
# x = np.exp(1j*(2*np.pi*f*n*T + phi))# hàm tính e
# xim = np.imag(x)# lấy phần thực
# plt.figure(1)# hàm chứ các thành phần của hình vẽ
# plt.plot(n*T,np.real(x))# vẽ đồ thị vecto từ n*T đến phần ảo của x
# plt.plot(n*T,xim)# vẽ đồ thị vecto từ n*T đến phần ảo của xim
# plt.axis([0,t,-1.1,1.1])# giới hạn đồ thị 0 ->t trục x, -1.1->1.1 trục y
# plt.xlabel('t(seconds)')# ghi tên dưới trục x
# plt.ylabel('amplitude')# ghi tên ở trục y
# plt.show()#hiển thị hình



# # ví dụ 6:
# f = 3
# N = 64
# samples = 100
# n = np.arange(64)# chia đều khoảng cách
# phi = 0
# x = np.exp(1j*(2*np.pi*f*n/N + phi))# hàm tính e
# xim = np.imag(x)# lấy phần thực
# plt.figure(1)# hàm chứ các thành phần của hình vẽ
# plt.plot(n,np.real(x))# vẽ đồ thị vecto từ n*T đến phần ảo của x
# plt.plot(n,xim)# vẽ đồ thị vecto từ n*T đến phần ảo của xim
# plt.axis([0,samples,-1.1,1.1])# giới hạn đồ thị 0 ->samples trục x, -1.1->1.1 trục y
# plt.xlabel('sample index')# ghi tên dưới trục x
# plt.ylabel('amplitude')# ghi tên ở trục y
# plt.show()#hiển thị hình



